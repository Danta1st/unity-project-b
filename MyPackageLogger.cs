﻿namespace UnityPackageB
{
    public class MyPackageLogger
    {
        public static void Log(string message)
        {
            var prefix = "B ";
            var msg = prefix + message;
            
            UnityPackageA.MyPackageLogger.Log(msg);

//            var prefix = typeof(MyPackageLogger).Namespace;
//            var mes = string.Format("[{0}] {1}", prefix, message);
//            
//            Debug.Log(mes);
        }
    }
}
